import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login'
import Guests from './views/Guests'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login/:company/:event/:invitation',
      props: true,
      name: 'login',
      component: Login
    },
    {
      path: '/convidados',
      name: 'guests',
      component: Guests
    }
  ]
})
