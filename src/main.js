import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store/'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import './registerServiceWorker'
import Raven from 'raven-js'
import RavenVue from 'raven-js/plugins/vue'

if (process.env.NODE_ENV !== 'development') {
  Raven
    .config('https://104936a617634a8a8d0553791d187b74@sentry.io/1278461', {
      release: '1.0.0-beta'
    })
    .addPlugin(RavenVue, Vue)
    .install()
}

const config = {
  apiKey: 'AIzaSyCCQTp1Q2WIvdzo-EOgSOsoP4kc-XrkUnY',
  authDomain: 'lista-buffet.firebaseapp.com',
  databaseURL: 'https://lista-buffet.firebaseio.com',
  projectId: 'lista-buffet',
  storageBucket: 'lista-buffet.appspot.com',
  messagingSenderId: '770954349012'
}

firebase.initializeApp(config)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
