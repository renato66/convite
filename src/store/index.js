import Vue from 'vue'
import Vuex from 'vuex'
// import createPersistedState from 'vuex-persistedstate'

import snackbar from './snackbar'
import user from './user'
import guests from './guests'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    guests,
    snackbar,
    user
  }
})

if (module.hot) {
  module.hot.accept(['./guests', './snackbar', './user'], () => {
    const guests = require('./guests').default
    const snackbar = require('./snackbar').default
    const user = require('./user').default

    store.hotUpdate({
      modules: {
        guests,
        snackbar,
        user
      }
    })
  })
}

export default store
