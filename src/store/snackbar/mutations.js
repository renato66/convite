export default {
  SHOW_MESSAGE (state, payload) {
    state.visibility = true
    if (payload.error !== undefined) {
      if (payload.error.message.includes('PERMISSION_DENIED')) {
        state.text = 'Permissão negada'
      }
    } else {
      state.text = payload.text
    }
    state.color = payload.color || ''
  },
  HIDE_MESSAGE (state, payload) {
    state.visibility = false
  }
}
