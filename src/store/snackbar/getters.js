export default {
  text (state) {
    return state.text
  },
  visibility (state) {
    return state.visibility
  },
  color (state) {
    return state.color === '' ? 'success' : state.color
  }
}
