import mutations from './mutations'
import getters from './getters'

export default {
  namespaced: true,
  state: {
    text: '',
    color: '',
    visibility: false
  },
  getters,
  mutations
}
