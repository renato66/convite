import Raven from 'raven-js'
export default {
  SET_USER (state, payload) {
    state.user.uid = payload.uid
    state.user.displayName = payload.displayName
    state.user.email = payload.email
    state.user.photoURL = payload.photoURL
    state.user.lastSignInTime = payload.metadata.lastSignInTime
  },
  SET_USER_RAVEN (state, payload) {
    if (process.env.NODE_ENV !== 'development') {
      Raven.setUserContext({
        email: payload.email,
        username: payload.displayName
      })
    }
  }
}
