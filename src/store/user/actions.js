import firebase from 'firebase/app'
import router from '@/router'
export default {
  setUser ({commit}, payload) {
    commit('SET_USER', payload)
    commit('SET_USER_RAVEN', firebase.auth().currentUser)
    router.push({
      name: 'guests'
    })
  },
  logout () {
    // window.localStorage.removeItem('vuex')
    router.push({
      name: 'login'
    })
    firebase.auth().signOut().then(() => {
    }).catch(error => {
      console.log(error)
    })
  }
}
