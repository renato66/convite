import mutations from './mutations'
import getters from './getters'
import actions from './actions'

export default {
  namespaced: true,
  state: {
    guests: [],
    drawer: false,
    listening: false,
    company: null,
    event: null,
    invitation: null,
    // company: '-LEyArnZNPDZnn6x90Zx',
    // event: '-LIy2D2FOW2sCP9_tJ4H',
    // invitation: '-LIy2D2FOW2sCP9_tJ4J',
    search: '',
    friends: []
  },
  getters,
  mutations,
  actions
}
