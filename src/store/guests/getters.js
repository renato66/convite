export default {
  guests (state) {
    return state.guests
  },
  userIsGoing (state, getters, rootState, rootGetters) {
    return state.guests.find(elem => {
      return elem['.key'] === rootGetters['user/user'].uid
    }) !== undefined
  },
  guestsFilter (state) {
    if (state.search === '') {
      return state.guests
    } else {
      const searchTerm = state.search.toUpperCase()
      return state.guests.filter(elem => {
        return elem.name.toUpperCase().includes(searchTerm)
      })
    }
  },
  listening (state) {
    return state.listening
  },
  event (state) {
    return state.event
  },
  company (state) {
    return state.company
  },
  invitation (state) {
    return state.invitation
  },
  search (state) {
    return state.search
  },
  friends (state) {
    return state.friends
  },
  ticketsLeft (state) {
    if (state.invitation.tickets === undefined) {
      return false
    } else {
      return state.invitation.tickets - state.guests.length
    }
  }
}
