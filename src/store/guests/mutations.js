export default {
  UNBIND_GUESTS (state) {
    state.guests = []
  },
  SET_LISTENING (state, payload) {
    state.listening = payload
  },
  UPDATE_GUEST (state, payload) {
    const index = state.guests.findIndex(elem => elem['.key'] === payload['.key'])
    state.guests.splice(index, 1, payload)
  },
  PUSH_GUEST (state, payload) {
    let length = state.guests.length
    if (length !== 0) {
      if (state.guests[length - 1].name < payload.name) {
        state.guests.push(payload)
      } else {
        // implement binary search
        state.guests.push(payload)
        if (state.guests[length - 1].name !== payload.name) {
          state.guests.sort((a, b) => {
            if (a.name < b.name) return -1
            if (a.name > b.name) return 1
            return 0
          })
        }
      }
    } else {
      state.guests.push(payload)
    }
  },
  SET_SEARCH (state, payload) {
    state.search = payload
  },
  DELETE_GUEST (state, payload) {
    state.guests = state.guests.filter(elem => elem['.key'] !== payload)
  },
  SET_FRIENDS (state, payload) {
    state.friends = payload
  },
  SET_COMPANY_KEY (state, payload) {
    state.company = {}
    state.company['.key'] = payload
  },
  SET_EVENT_KEY (state, payload) {
    state.event = {}
    state.event['.key'] = payload
  },
  SET_INVITATION_KEY (state, payload) {
    state.invitation = {}
    state.invitation['.key'] = payload
  },
  SET_INVITATION (state, payload) {
    state.invitation = payload
  }
}
