import firebase from '@firebase/app'
export default {
  unbindGuests: ({ getters, commit }) => {
    firebase.database().ref(`company/${getters.company['.key']}/guests/${getters.event['.key']}/invitations/${getters.invitation['.key']}/guest`).off()
    commit('UNBIND_GUESTS')
    commit('SET_LISTENING', false)
  },
  bindGuests: ({ getters, rootGetters, commit, dispatch }) => {
    if (getters.company['.key'] === null || getters.event['.key'] === null || getters.invitation['.key'] === null) {
      commit('snackbar/SHOW_MESSAGE', {
        color: 'error',
        text: 'Erro interno'
      }, { root: true })
    } else {
      if (getters.listening) return
      commit('SET_LISTENING', true)
      firebase.database().ref(`company/${getters.company['.key']}/guests/${getters.event['.key']}/invitations/${getters.invitation['.key']}/guest`)
        .orderByChild('name').on('child_added', snapshot => {
          let guest = snapshot.val()
          guest['.key'] = snapshot.key
          commit('PUSH_GUEST', guest)
        })
      firebase.database().ref(`company/${getters.company['.key']}/guests/${getters.event['.key']}/invitations/${getters.invitation['.key']}/guest`)
        .orderByChild('name').on('child_changed', snapshot => {
          let guest = snapshot.val()
          guest['.key'] = snapshot.key
          commit('UPDATE_GUEST', guest)
        })
      firebase.database().ref(`company/${getters.company['.key']}/guests/${getters.event['.key']}/invitations/${getters.invitation['.key']}/guest`)
        .on('child_removed', snapshot => {
          const deletedGuest = snapshot.key
          commit('DELETE_GUEST', deletedGuest)
        })
    }
  },
  removeGuest: ({ getters }, payload) => {
    firebase.auth().currentUser.getIdToken().then(token => {
      fetch(`https://us-central1-lista-buffet.cloudfunctions.net/app/remove-guest/${getters.company['.key']}/${getters.event['.key']}/${getters.invitation['.key']}`, {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify({
          key: payload['.key']
        })
      }).then(response => {
        if (response.status === 200) {
          return response
        } else {
          const error = new Error(response.statusText)
          error.response = response
          throw error
        }
      }).then(myJson => {
        console.log(myJson)
      }).catch(error => {
        console.log(error)
        console.log('error.response.status === 422', error.response.status === 422)
      })
    })
  },
  setKeys: ({ getters, commit }, payload) => {
    commit('SET_COMPANY_KEY', payload.company)
    commit('SET_EVENT_KEY', payload.event)
    commit('SET_INVITATION_KEY', payload.invitation)
    firebase.database().ref(`company/${getters.company['.key']}/guests/${getters.event['.key']}/invitations/${getters.invitation['.key']}`).once('value', snapshot => {
      let invitation = snapshot.val()
      invitation['.key'] = snapshot.key
      console.log(invitation)
      commit('SET_INVITATION', invitation)
    })
  },
  addGuest: ({ getters }, payload) => {
    let name = []
    let avatar = payload.avatar
    let uid = payload.uid
    for (let char of payload.name.split(' ')) {
      name.push(char[0].toUpperCase() + char.slice(1))
    }
    name = name.join(' ')
    firebase.auth().currentUser.getIdToken().then(token => {
      fetch(`https://us-central1-lista-buffet.cloudfunctions.net/app/add-guest/${getters.company['.key']}/${getters.event['.key']}/${getters.invitation['.key']}`, {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify({
          name,
          avatar,
          uid
        })
      }).then(response => {
        if (response.status === 200) {
          return response.json()
        } else {
          const error = new Error(response.statusText)
          error.response = response
          throw error
        }
      }).then(myJson => {
        window.lol = myJson
        console.log(myJson)
      }).catch(error => {
        console.log(error)
        console.log('error.response.status === 422', error.response.status === 422)
      })
    })
  }
}
